package pl.codementors.JavaFXAnimations;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import pl.codementors.JavaFXAnimations.shapes.Star;

import java.util.Random;

public class AnimationsMain extends Application {
    private final static int sceneSize = 600;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Star star = new Star(100, 300, 300);
        star.setFill(Color.BLUE);
        Text text = new Text("homeworks are awesome");
        text.setFill(Color.GREEN);
        text.setLayoutX(200);
        text.setY(150);

        Group root = new Group();
        root.getChildren().add(star);
        root.getChildren().add(text);


        createTopLine(root);
        createBottomLine(root);
        createLeftLine(root);
        createRightLine(root);



        Scene scene = new Scene(root, sceneSize, sceneSize, Color.BLACK);
        stage.setScene(scene);
        stage.show();
    }

    private void createRightLine(Group root) {
        int x = 460;
        int y = 10;
        for (int i = 0; i < 4; i++) {
            root.getChildren().add(createRandomRectangle(x, y));
            root.getChildren().add(createRandomCircle(x, y));
            y += 150;
        }
    }

    private void createLeftLine(Group root) {
        int x = 10;
        int y = 10;
        for (int i = 0; i < 4; i++) {
            root.getChildren().add(createRandomRectangle(x, y));
            root.getChildren().add(createRandomCircle(x, y));
            y += 150;
        }
    }

    private void createBottomLine(Group root) {
        int x = 10;
        int y = 540;
        for (int i = 0; i < 4; i++) {
            root.getChildren().add(createRandomRectangle(x, y));
            root.getChildren().add(createRandomCircle(x, y));
            x += 150;
        }
    }

    private void createTopLine(Group root) {
        int x = 10;
        int y = 10;
        for (int i = 0; i < 4; i++) {
            root.getChildren().add(createRandomRectangle(x, y));
            root.getChildren().add(createRandomCircle(x, y));
            x += 150;
        }
    }

    private Circle createRandomCircle(int x, int y) {
        Random random = new Random();
        Circle circle = new Circle();
        circle.setFill(Color.color(random.nextDouble(), random.nextDouble(), random.nextDouble()));
        final int circleRadius = 25;
        circle.setRadius(circleRadius);
        circle.setCenterX(x + 100);
        circle.setCenterY(y + 25);
        return circle;
    }


    private Rectangle createRandomRectangle(int x, int y) {
        Random random = new Random();
        Rectangle rectangle = new Rectangle();
        rectangle.setFill(Color.color(random.nextDouble(), random.nextDouble(), random.nextDouble()));
        final int rectangleSize = 50;
        rectangle.setHeight(rectangleSize);
        rectangle.setWidth(rectangleSize);
        rectangle.setX(x);
        rectangle.setY(y);
        return rectangle;
    }

}
